#include <iostream>

#include <log4cxx/logger.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/simplelayout.h>
#include <log4cxx/logmanager.h>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {

    log4cxx::Logger::getRootLogger()->info("Hello, log4cxx");

    return 0;
}