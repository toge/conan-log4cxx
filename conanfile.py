from conans import ConanFile, CMake, tools

class Log4cxxConan(ConanFile):
    name            = "log4cxx"
    version         = "0.10.0_20200504"
    license         = "Apache-2.0"
    author          = "toge.mail@gmail.com"
    url             = "https://bitbucket.org/toge/conan-log4cxx/"
    homepage        = "https://github.com/apache/logging-log4cxx"
    description     = "Apache log4cxx is a logging framework for C++ patterned after Apache log4j, which uses Apache Portable Runtime for most platform-specific code and should be usable on any platform supported by APR."
    topics          = ("logging", "apache")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = {"shared": False}
    generators      = "cmake"
    requires        = ["apr/1.7.0", "apr-util/1.6.1"]

    def source(self):
        self.run("git clone https://github.com/apache/logging-log4cxx/")
        self.run("cd logging-log4cxx; git checkout 400f15c3052fed61e1ae8363a61debbb93de45a9")

        tools.replace_in_file("logging-log4cxx/CMakeLists.txt", "include(CTest)",
                              '''include(CTest)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="logging-log4cxx")
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="logging-log4cxx/src/main/include")
        if self.settings.os == "Windows":
            self.copy("*.lib",   dst="lib",     keep_path=False)
            self.copy("*.dll",   dst="bin",     keep_path=False)
        if self.settings.os == "Linux":
            self.copy("*.so",    dst="lib",     keep_path=False)
            self.copy("*.a",     dst="lib",     keep_path=False)
        if self.settings.os == "Macos":
            self.copy("*.so",    dst="lib",     keep_path=False)
            self.copy("*.a",     dst="lib",     keep_path=False)
            self.copy("*.dylib", dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["log4cxx"]
